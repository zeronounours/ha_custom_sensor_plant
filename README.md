# Plants

Extend the builtin plant sensor to make additional data available:
- Min and max values when defined for problem checks

## Installation

1. Clone the repository within `custom_components`
```shell
cd /config/custom_components
git clone https://gitlab.com/zeronounours/ha_custom_sensor_plant.git plant
```
2. Restart Home Assistant

## Configuration

The configuration of the component is the same as the builtin `plant`
integration: https://www.home-assistant.io/integrations/plant/
